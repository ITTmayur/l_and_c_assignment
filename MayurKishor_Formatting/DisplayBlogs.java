package com.itt.lc;

//import statements should be sorted with the most fundamental packages first, 
//and grouped with associated packages together and one blank line between groups.
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DisplayBlogs {

	public static void fetchBlogs(String blogName, String range[]) {

		String blogJsonData = "";
		URL url;
		try {
			url = new URL("https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + range[1] + "&start=" + range[0]);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.connect();
			Scanner sc = new Scanner(url.openStream());
			while (sc.hasNext()) {
				blogJsonData += sc.nextLine();
			}
			sc.close();
			while (blogJsonData.startsWith("var tumblr_api_read = ")) {
				blogJsonData = blogJsonData.substring("var tumblr_api_read = ".length());
				blogJsonData = blogJsonData.replace(";", "");
				break;
			}
			conn.disconnect();

			displayBlogDetails(blogJsonData, range);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void displayBlogDetails(String blogJsonData, String range[]) {

		JSONParser jsonParser = new JSONParser();
		JSONObject blogJsonObject;
		try {
			blogJsonObject = (JSONObject) jsonParser.parse(blogJsonData);
			JSONObject jsonTumblelogObject = (JSONObject) blogJsonObject.get("tumblelog");
			System.out.println("\nTitle : " + blogJsonObject.get("title"));
			System.out.println("Name : " + jsonTumblelogObject.get("name"));
			System.out.println("Description : " + jsonTumblelogObject.get("description"));
			System.out.println("Number Of Posts : " + blogJsonObject.get("posts-total") + "\n");

			JSONArray postsJsonArray = (JSONArray) blogJsonObject.get("posts");
			String rangeNumber = range[0];
			int postRange = Integer.parseInt(rangeNumber);

			Iterator<JSONObject> postRangeIterator = postsJsonArray.iterator();
			while (postRangeIterator.hasNext()) {
				JSONObject post = postRangeIterator.next();
				String imageUrl = (String) post.get("photo-url-1280");
				System.out.println(postRange++ + ". " + imageUrl);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter The Tumblr Blog Name : ");
		String blogName = scan.next();

		String range[] = new String[2];
		System.out.println("\nEnter The Range : ");
		String requiredRange = scan.next();
		range = requiredRange.split("-");
		if (Integer.parseInt(range[1]) > 50) {
			System.out.println("\nPlease Enter The Range Less Than or Equal to 50 : ");
			requiredRange = scan.next();
			range = requiredRange.split("-");
		}
		fetchBlogs(blogName, range);
		scan.close();
	}
}

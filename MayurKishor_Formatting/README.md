CODE FORMATTING RULES:

1)Write only one statement per line.

2)Write only one declaration per line.

3)Continous indentation : Placing the closing parenthesis vertically linear to the opening parenthesis

4)Add at least one blank line between method definitions and property definitions.

5)Place the comment on a separate line, not at the end of a line of code.

6)Insert one space between the comment delimiter (//) and the comment text

7)Code Grouping: Place the codes of line perfroming a certain tasks together in one block

8)Limit line length :Avoid writing long horizontal lines of codes

9)Closing brackets Consistenly: If brackets are closed in same line, then it should be consistent throughout the code.
There should be following correct indentation when code contains inner blocks like if ,if-else, while(), do catch(), for() loops, switch cases etc,.

10)Blank lines: Used to seperate two methods or blocks for better readibility

11)Vertical spacing should be consistent throughout the code 

12)Length of functions should not be very large.

13)Insert a new line before an opening brace

14)Insert new lines in control statements(if else)

15)Insert a new line inside an empty block

16)One vertical line spacing between import section and class definition.

17)One vertical line spacing between class declaration and variable declaration. 

18)One horizontal spacing between initialisation variables, local variable and in assigning them

19)One horizontal space between each and every operator between operands.

20)No spacing before brackets. Example : "n=random.randint(1,100)"
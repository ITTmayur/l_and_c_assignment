package com.itt.api;

import java.util.Scanner;

public class GeoCoding {

    public static void main(String[] args) {
        System.out.println("Please Enter Place: ");
        Scanner scan = new Scanner(System.in);
        String place = scan.nextLine();
        GeocodeApiAdapter GeocodeApiAdapter = new GeocodeApiAdapter();
        GeocodeApiAdapter.getData(place);
    }
}

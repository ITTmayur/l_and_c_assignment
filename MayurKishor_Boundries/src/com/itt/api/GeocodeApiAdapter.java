package com.itt.api;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GeocodeApiAdapter implements GeocodeApi {

    @Override
    public void getData(String place) {

        Scanner scan = new Scanner(System.in);
        Coordinates coordinates = new Coordinates();
        String locationJsonData = "";
        URL url;
        try {
            url = new URL(
                "https://api.opencagedata.com/geocode/v1/json?q=" + place +
                    "&key=20a46f4d85014357a21125476831b69b&language=en&pretty=1");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            Scanner sc = new Scanner(url.openStream());
            while (sc.hasNext()) {
                locationJsonData += sc.nextLine();
            }
            sc.close();
            connection.disconnect();

            JSONParser jsonParser = new JSONParser();
            JSONObject locationJsonObject;
            try {
                locationJsonObject = (JSONObject) jsonParser.parse(locationJsonData);
                JSONArray locationJsonArray = (JSONArray) locationJsonObject.get("results");
                JSONObject resultsObject = (JSONObject) locationJsonArray.get(0);
                JSONObject geometryObject = (JSONObject) resultsObject.get("geometry");
                double latitude = (double) geometryObject.get("lat");
                coordinates.setLatitude(latitude);
                double longitude = (double) geometryObject.get("lng");
                coordinates.setLongitude(longitude);
                LocationCoordinates locationCoordinates = new LocationCoordinates();
                locationCoordinates.display(coordinates);
            } catch (ParseException parseException) {
                System.out.println("There Was Some Parsing Error!!! Please Try Again");
            }
        } catch (IOException ioException) {
            System.out.println("Please Enter Place Name without Giving Space!!! Re-Enter: ");
            place = scan.nextLine();
            getData(place);
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            System.out.println("There is No Place With Name : " + place + " Please Verify And Re-Enter: ");
            place = scan.nextLine();
            getData(place);
        }
    }
}
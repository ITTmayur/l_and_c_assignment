package com.itt.api;

public class LocationCoordinates {
    public void display(Coordinates coordinates) {
        System.out.println("Latitude : " + coordinates.getLatitude());
        System.out.println("Longitude : " + coordinates.getLongitude());
    }
}

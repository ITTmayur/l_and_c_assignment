package com.itt.atm;

import java.util.Scanner;

public class AutheticatePin {
    
    int attempts = 2;

    public boolean authenticate(AtmCard atmCard) {
        Scanner scan = new Scanner(System.in);
        boolean isAuthenticated = false;
        System.out.println("\nEnter Pin: ");
        int pin = scan.nextInt();
        while (attempts != 0) {
            if (pin == atmCard.getPin()) {
                isAuthenticated = true;
                break;
            } else {
                attempts--;
                System.out.println("Incorrect Pin !!! Please Re-Enter: ");
                pin = scan.nextInt();
                if (pin == atmCard.getPin()) {
                    isAuthenticated = true;
                    break;
                }
            }
        }
        if(attempts == 0) {
            System.out.println(">>> Oops !!! Card Blocked.");
        }
        return isAuthenticated;
    }
}

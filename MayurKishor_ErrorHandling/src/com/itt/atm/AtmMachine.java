package com.itt.atm;

import java.util.Scanner;

import com.itt.exception.InsufficientBalanceException;
import com.itt.exception.InvalidAmountException;
import com.itt.exception.NetworkException;

public class AtmMachine {

    public static void main(String args[]) {

        Scanner scan = new Scanner(System.in);
        AtmCard atmCard = new AtmCard();
        AtmTransaction atmTransaction = new AtmTransaction();
        AutheticatePin authenticatePin = new AutheticatePin();
        double amount = 0.0, withdrawalAmount = 0.0;
        boolean isNetwork = true;
        try {
            if (isNetwork) {
                System.out.println("Welcome !!!");
                System.out.println();
                System.out.println("Set your 4 digit pin: ");
                int pin = scan.nextInt();
                atmCard.setPin(pin);
                System.out.println("\nYour Pin: " + atmCard.getPin());
                boolean isAuthenticated = authenticatePin.authenticate(atmCard);
                if (isAuthenticated) {
                    System.out.println("Select Option: ");
                    System.out.println("\n1) Deposite \n2) Withdraw \n3) Balance Enquiry \n4) Cancel");
                    int choice;
                    do {
                        System.out.println("\nEnter Choice: ");
                        choice = scan.nextInt();
                        try {
                            switch (choice) {
                            case 1:
                                System.out.println("\nEnter amount to deposite: ");
                                amount = scan.nextDouble();
                                atmCard.setBalance(amount);
                                System.out.println(amount + " Successfully Deposited.");
                                System.out.println("\nBalance After Deposite: " + atmTransaction.balanceEnquiry(atmCard));
                                break;

                            case 2:
                                System.out.print("Enter amount to withdraw: ");
                                amount = scan.nextDouble();
                                withdrawalAmount = atmTransaction.withdraw(amount, atmCard);
                                System.out.println(">>> Collect " + withdrawalAmount + " cash");
                                break;

                            case 3:
                                System.out.println(">>> Current Balance: " + atmTransaction.balanceEnquiry(atmCard));
                                break;

                            case 4:
                                System.out.println("You Cancelled the Transaction !!!");
                                break;

                            default:
                                System.out.println("Please Enter Valid Choice !!!");
                            }
                        } catch (InvalidAmountException e) {
                            System.out.println(e.getMessage());
                        } catch (InsufficientBalanceException e) {
                            System.out.println(e.getMessage());
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    while (choice != 4);
                }
            } else {
                throw new NetworkException(">>> There is a Network Problem");
            }
        } catch (NetworkException e) {
            System.out.println(e.getMessage());
        }
    }
}

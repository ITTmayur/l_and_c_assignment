
package com.itt.atm;

import com.itt.exception.InsufficientBalanceException;
import com.itt.exception.InvalidAmountException;

public class AtmTransaction {

    private double atmBalance = 20000;

    public double withdraw(double amount, AtmCard atmCard)
        throws InvalidAmountException, InsufficientBalanceException {
        if (atmBalance <= 0) {
            throw new InvalidAmountException(">>> There is " + atmBalance + " cash available in ATM");
        }

        if (atmCard.getBalance() < amount) {
            throw new InsufficientBalanceException(">>> " + amount + " is not available in your account");
        }
        double balance = atmCard.getBalance() - amount;
        atmCard.setBalance(balance);
        return amount;
    }

    public double balanceEnquiry(AtmCard atmCard) {
        return atmCard.getBalance();
    }
}

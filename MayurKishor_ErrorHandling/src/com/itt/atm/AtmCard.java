package com.itt.atm;

public class AtmCard {

    private int pin;
    private double balance;

    public void setPin(int pin) {
        this.pin = pin;
    }
    
    public int getPin() {
        return this.pin;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    public double getBalance() {
        return this.balance;
    }
}


package com.itt.exception;

public class InsufficientBalanceException extends Exception {

    public InsufficientBalanceException(String msg){
        super(msg);
    }
}

import java.util.Scanner;

public class GuessCorrectNumber {

	public static boolean guessingCorrectNumber(int inputNumber) {
		if (inputNumber >= 1 && inputNumber <= 100) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		int randomNumber = (int) (Math.random() * 99 + 1);
		System.out.println("random number " + randomNumber);
		boolean isCorrectNumber = false;
		System.out.println("Guess a number between 1 to 100 : ");
		int inputNumber = scan.nextInt();
		int counter = 0;
		while (!isCorrectNumber) {
			if (!guessingCorrectNumber(inputNumber)) {
				System.out.println("I wont count this one Please enter a number between 1 to 100 : ");
				inputNumber = scan.nextInt();
			} else {
				counter += 1;
				if (inputNumber < randomNumber) {
					System.out.println("Too Low. Guess Again !!!");
					break;
				} else if (inputNumber > randomNumber) {
					System.out.println("Too High. Guess Again !!!");
					break;
				} else {
					System.out.println("You guessed it in " + counter + " guesses");
					isCorrectNumber = true;
				}
			}
		}
		scan.close();
	}
}

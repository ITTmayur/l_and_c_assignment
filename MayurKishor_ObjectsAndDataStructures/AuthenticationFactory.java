package com.itt;

public class AuthenticationFactory {

    public Authentication getAuth(String authType) {
        if(authType == null) {
            return null;
        }
        
        if(authType.equalsIgnoreCase("OTP_AUTH")) {
            return new OtpAuthentication();
        }
        
        if(authType.equalsIgnoreCase("ID_AND_PASS_AUTH")) {
            return new IdAndPasswordAuthentication();
        }
        return null;
    }
}

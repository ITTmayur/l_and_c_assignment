package com.itt;

import java.util.Scanner;

public class Customer {
    private String firstName;
    private String lastName;
    private Wallet myWallet;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public float getPayment(float bill) {
        AuthenticationFactory authentication = new AuthenticationFactory();
        Authentication isAuthenticated = null;
        Scanner scan = new Scanner(System.in);
        System.out.println("Please Choose the Option for payment : ");
        System.out.println("\n1) Use phone number \n2) Use Id and Password");
        int choice;
        String authType = "";
        do {
            System.out.println("\nEnter Choice :");
            choice = scan.nextInt();
            switch (choice) {
            case 1:
                authType = "OTP_AUTH";
                isAuthenticated = authentication.getAuth(authType);
                isAuthenticated.authenticate();
                break;

            case 2:
                authType = "ID_AND_PASS_AUTH";
                isAuthenticated = authentication.getAuth(authType);
                isAuthenticated.authenticate();
                break;
                
            case 3:
                break;
                
            default:
                System.out.println("\nPlease Enter Valid Choice");

            }
        } while (choice != 3);
        
        float paidAmount = 0;
        if (isAuthenticated != null) {
            myWallet = new Wallet();
            if (myWallet.getTotalMoney() >= bill) {
                myWallet.subtractMoney(bill);
                paidAmount = bill;
            }
        } else {
            System.out.println("\nPlease Enter the Valid Credentials !!!");
        }
        return paidAmount;
    }
}

package com.itt;

public interface Authentication {
   public boolean authenticate();
}

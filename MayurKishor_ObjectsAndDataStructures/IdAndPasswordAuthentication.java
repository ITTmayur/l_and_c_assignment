package com.itt;

import java.util.Scanner;

public class IdAndPasswordAuthentication implements Authentication {
   
    @Override
    public boolean authenticate() {
        Wallet wallet = new Wallet();
        boolean isAuthenticated = false;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Enter Id :");
        int id = scan.nextInt();
        
        System.out.println("Enter Password");
        int password = scan.nextInt();
        
        if(wallet.getId() == id && wallet.getPassword() == password) {
            isAuthenticated = true;
        } 
        return isAuthenticated;
    } 
}

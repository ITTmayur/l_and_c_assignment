package com.itt;

public class Wallet {
    private int id = 111;
    private int password = 1234;
    private String phoneNumber;
    private int otp;
    private float value = 10.00f;

    public int getId() {
        return id;
    }

    public int getPassword() {
        return password;
    }
    
    public float getTotalMoney() {
        return value;
    }
    
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public void setOtp(int otp) {
        this.otp = otp;
    }
    
    public int getOtp() {
        return otp;
    }

    public void setTotalMoney(float newValue) {
        value = newValue;
    }

    public void addMoney(float deposit) {
        value += deposit;
    }

    public void subtractMoney(float debit) {
        value -= debit;
    }
}

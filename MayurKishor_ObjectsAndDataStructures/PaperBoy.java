package com.itt;

public class PaperBoy {
    public static void main(String[] args) {
        float bill = 2.00f;
        Customer customer = new Customer();
        float paidAmount = customer.getPayment(bill);
        if (paidAmount == bill) {        
             System.out.println("Thank You");   
        } else {         
            System.out.println("No sufficient amount");    
        }
    }
}

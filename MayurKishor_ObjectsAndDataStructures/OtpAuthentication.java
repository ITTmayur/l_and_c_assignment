package com.itt;

import java.util.Random;
import java.util.Scanner;

public class OtpAuthentication implements Authentication {

    @Override
    public boolean authenticate() {
        Wallet wallet = new Wallet();
        boolean isAuthenticated = false;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("\nEnter Phone Number: ");
        String phoneNumber = scan.next();
        wallet.setPhoneNumber(phoneNumber);
        Random rand = new Random();
        int generatedOtp = rand.nextInt(1000);
        wallet.setOtp(generatedOtp);
        System.out.println("\nYour OTP: " + generatedOtp);
        
        System.out.println("Enter OTP: ");
        int otp = scan.nextInt();
        if(wallet.getOtp() == otp) {
            isAuthenticated = true;
        }
        return isAuthenticated;
    }
}
